/*******************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 * 
 *    This file is part of LogMap.
 * 
 *    LogMap is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 * 
 *    LogMap is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 * 
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with LogMap.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package uk.ac.ox.krr.logmap2.reasoning.deprecated;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.util.DLExpressivityChecker;

import java.util.Calendar;
import java.util.Set;
//import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;


public class PelletAccess{
	
	
	private PelletReasoner pellet;
	
	private OWLOntology ontoBase;
	private OWLOntologyManager ontoManager;
	private IRI iri;
	
	private OWLDataFactory datafactory;
	
	private long init, fin;
	
	
	public PelletAccess(OWLOntologyManager ontoManager, OWLOntology onto, boolean classify) throws Exception {
		
		ontoBase=onto;
		
		iri=onto.getOntologyID().getOntologyIRI();
		
		this.ontoManager=ontoManager;
		
		datafactory=ontoManager.getOWLDataFactory();
		
		//This will avoid obtaining already known disjoint
		//removeExplicitDisjointness();
				
		System.out.print("Set up reasoner... ");
		setUpReasoner();

		if (classify)
			classifyOntology();
		
	}
	
	
	public PelletAccess(OWLOntologyManager ontoManager, OWLOntology onto) throws Exception {
		
		this(ontoManager, onto, true);
		
	}
	
	public OWLOntology getBaseOntology(){
		return ontoBase;
	}
	
	public boolean isSatisfiable(OWLClass cls){
		return pellet.isSatisfiable(cls);
	}
	
	public PelletReasoner getReasoner(){
		return pellet;
	}
	
	private void setUpReasoner(){
		//PelletReasonerFactory.getInstance().createReasoner(ontoBase);
		pellet = new Pellet_adapted(ontoBase);
		
		Set<OWLOntology> importsClosure = ontoManager.getImportsClosure(ontoBase);
        //hermit.loadOntologies(importsClosure);
		//hermit.loadOntology(ontoManager, ontoBase, null);
		
	
        DLExpressivityChecker checker = new DLExpressivityChecker(importsClosure);
        System.out.println("Expressivity Ontology: " + checker.getDescriptionLogicName());
		
		
	}
	
	public void classifyOntologyNoProperties(){
    	classifyOntology(false);
    }
	
    public void classifyOntology(){
    	classifyOntology(true);
    }
	
	public void classifyOntology(boolean classproperties){
		init=Calendar.getInstance().getTimeInMillis();
        System.out.print("Classifying Ontology with Pellet... ");
		pellet.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		if (classproperties){
			pellet.precomputeInferences(InferenceType.DATA_PROPERTY_HIERARCHY);
			pellet.precomputeInferences(InferenceType.OBJECT_PROPERTY_HIERARCHY);
		}
		pellet.precomputeInferences(InferenceType.DISJOINT_CLASSES);//Only explicit, we have extended method!
		
		fin = Calendar.getInstance().getTimeInMillis();
		System.out.println("Done, Time (s): " + (float)((double)fin-(double)init)/1000.0);
	}
	
	
	
	public Set<OWLClass> getUnsatisfiableClasses() throws Exception{
		
		
		Set<OWLClass> set;
	
		//Now the reasoner return a node structure. A Node contains the
		//set of entities which are equivalent
		Node<OWLClass> node = pellet.getUnsatisfiableClasses();
	
		//set = node.getEntities();
		set = node.getEntitiesMinusBottom();
		
		//set.remove(datafactory.getOWLNothing());
		
		if (!set.isEmpty()) {
			//System.out.println("The following classes are unsatisfiable: ");
			//for(OWLClass cls : set) {
			//	System.out.println(" " + cls);
			//}
			System.out.println("There are '" + set.size() + "' UNSATisfiable classes.");
		}
		else{
			System.out.println("There are '0' unsatisfiable classes.");
		}
		
		return set;
		
	}
	
}


