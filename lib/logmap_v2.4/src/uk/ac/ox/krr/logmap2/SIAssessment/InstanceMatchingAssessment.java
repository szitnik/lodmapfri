/**
 * *****************************************************************************
 * Copyright 2012 by the Department of Computer Science (University of Oxford)
 *
 * This file is part of LogMap.
 *
 * LogMap is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * LogMap is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LogMap. If not, see <http://www.gnu.org/licenses/>.
 * ****************************************************************************
 */
package uk.ac.ox.krr.logmap2.SIAssessment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import uk.ac.ox.krr.logmap2.Parameters;

import uk.ac.ox.krr.logmap2.indexing.IndexManager;
import uk.ac.ox.krr.logmap2.mappings.MappingManager;
import uk.ac.ox.krr.logmap2.utilities.Utilities;

public class InstanceMatchingAssessment {

    protected IndexManager index;
    protected MappingManager mapping_manager;

    protected final int EMPTY_TYPES = 0;
    protected final int ONE_TYPE_EMPTY = 1;

    protected final int COMPATIBLE_TYPES = 2;
    protected final int INCOMPATIBLE_TYPES = 3;

    protected final int SAME_TYPES = 4;
    protected final int SUB_TYPES = 5;

    protected int compatibility = 0;

    public InstanceMatchingAssessment(IndexManager index, MappingManager mapping_manager) {

        this.index = index;
        this.mapping_manager = mapping_manager;

    }

    protected int areInstancesCompatible(int ident1, int ident2) {

        Set<Integer> types1 = index.getIndividualClassTypes4Identifier(ident1);
        Set<Integer> types2 = index.getIndividualClassTypes4Identifier(ident2);

        Set<Integer> mapped_types1 = new HashSet<Integer>();

        if (types1.isEmpty() && types2.isEmpty()) {
            return EMPTY_TYPES;
        }
        if (types1.isEmpty() || types2.isEmpty()) {
            return ONE_TYPE_EMPTY;
        }

        for (int cls1 : types1) {
            if (mapping_manager.getAnchors().containsKey(cls1)) {
                mapped_types1.addAll(mapping_manager.getAnchors().get(cls1));
            }
        }

        if (haveSameClassTypes(mapped_types1, types2)) {
            return SAME_TYPES;
        }

        for (int cls1 : types1) {
            for (int cls2 : types2) {
                if (mapping_manager.isMappingInConflictWithFixedMappings(cls1, cls2)) {
                    return INCOMPATIBLE_TYPES;
                }
            }
        }

        //Subtypes
        if (areAllSubTypes(types1, types2)) {
            return SUB_TYPES;
        }

        return COMPATIBLE_TYPES;

    }

    public double getConfidence4Compatibility(int ident1, int ident2) {

        compatibility = areInstancesCompatible(ident1, ident2);

        switch (compatibility) {
            case EMPTY_TYPES:
                return 0.90; //0.85
            case ONE_TYPE_EMPTY:
                return 0.90;
            case SAME_TYPES:
                return 0.75;//0.75
            case SUB_TYPES:
                return 0.80;
            case COMPATIBLE_TYPES:
                return 0.85;
            case INCOMPATIBLE_TYPES:
                return 2.0;
            default:
                return 2.0;
        }
    }

    public double getCompatibilityFactor(int ident1, int ident2) {

        //compatibility has been set when calling the above method
        switch (compatibility) {
            case EMPTY_TYPES:
                return 0.50;
            case ONE_TYPE_EMPTY:
                return 0.50;
            case SAME_TYPES:
                return 1.0;
            case SUB_TYPES:
                return 0.90;
            case COMPATIBLE_TYPES:
                return 0.70;
            case INCOMPATIBLE_TYPES:
                return 0.0; //no compatible 
            default:
                return 0.0;
        }
    }

    protected boolean haveSameClassTypes(Set<Integer> types1, Set<Integer> types2) {

        if (types1.size() > 0 && types2.size() > 0) {
            return types1.equals(types2);
        }

        return false;
    }

    protected boolean areAllSubTypes(Set<Integer> types1, Set<Integer> types2) {

        for (int cls1 : types1) {
            for (int cls2 : types2) {
                if (!index.isSubClassOf(cls1, cls2) && !index.isSubClassOf(cls2, cls1)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @author DBAL
     * @param ident1
     * @param ident2
     * @param anchors
     * @param debug
     * @return
     */
    public double getNeighbourhoodSimilarityFactor(int ident1, int ident2, Map<Integer, Set<Integer>> anchors) {

        double similarity_factor = 0.0;
        int num_classes = 0;

        Set<Integer> types1 = index.getIndividualClassTypes4Identifier(ident1);
        Set<Integer> types2 = index.getIndividualClassTypes4Identifier(ident2);
        
        if (Parameters.debug) {
            System.out.println("INSTANCE MATCHING");
            System.out.println("Instances: " + index.getName4IndividualIndex(ident1) + "(" + ident1 + ")" + " - " + index.getName4IndividualIndex(ident2) + "(" + ident2 + ")");
        }
        
        
        

        for (int cls1 : types1) {

            if (Parameters.debug) {
//                sub classes nad super classes mappping
                System.out.println("getSubsetOfSubClasses4Identifier " + index.getIRIStr4ConceptIndex(cls1) + ": " + index.getSubsetOfSubClasses4Identifier(cls1));
                System.out.println("getSubsetOfSuperClasses4Identifier " + index.getIRIStr4ConceptIndex(cls1) + ": " + index.getSubsetOfSuperClasses4Identifier(cls1));
                System.out.println("Instance 1 type: " + index.getIRIStr4ConceptIndex(cls1));
            }

            for (int cls2 : types2) {
                if (Parameters.debug) {
//                sub classes nad super classes mappping
                    System.out.println("getSubsetOfSubClasses4Identifier " + index.getIRIStr4ConceptIndex(cls2) + ": " + index.getSubsetOfSubClasses4Identifier(cls2));
                    
                    
                    
                    
                    System.out.println("getSubsetOfSuperClasses4Identifier " + index.getIRIStr4ConceptIndex(cls2) + ": " + index.getSubsetOfSuperClasses4Identifier(cls2));
                    System.out.println("Instance 2 type: " + index.getIRIStr4ConceptIndex(cls2));
                }

                if (anchors.containsKey(cls1)) {

                    Set<Integer> entryset = anchors.get(cls1);

                    if (entryset.contains(cls2)) {

                        if (Parameters.debug) {
//                            System.out.println("Classes: " + index.getIRIStr4ConceptIndex(cls1) + "(" + cls1 + ")"
//                                    + " - " + index.getIRIStr4ConceptIndex(cls2) + "(" + cls2 + "): "
//                                    + mapping_manager.getConfidence4Mapping(cls1, cls2));
                            System.out.println("DBAL - Confidence4Mapping: " + mapping_manager.getConfidence4Mapping(cls1, cls2));

                            System.out.println(Arrays.toString(entryset.toArray()));
//                            System.out.println(mapping_manager.getConfidence4Mapping(cls1, cls2));
//                            System.out.println(mapping_manager.isMappingInConflictWithFixedMappings(cls1, cls2));
//                            System.out.println(index.areDisjoint(cls1, cls2));
//                            System.out.println(index.arePartiallyDisjoint(cls1, cls2));
//                            System.out.println(index.areEquivalentClasses(cls1, cls2));
//                            System.out.println(index.areSiblings(cls1, cls2));
                        }

                        similarity_factor += mapping_manager.getConfidence4Mapping(cls1, cls2);

                    }
                }
                num_classes++;
            }
        }

        if (num_classes > 0) {
            return (similarity_factor / num_classes) * 10;
        }

        if (Parameters.debug) {
            System.out.println("DBAL - similarity_factor: " + similarity_factor);
        }
        return similarity_factor;
    }

}
