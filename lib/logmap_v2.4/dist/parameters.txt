debug|false
print_output|true
good_isub_anchors|0.98
good_isub_candidates|0.95
max_ambiguity|4
good_ambiguity|2
# begin intersection of ontology sets - for big ontologies
use_overlapping|true

######### DBAL #########
good_confidence|0.50
good_sim_coocurrence|0.08
min_conf_pro_map|0.80

good_instance_confidence_map|0.96

# 0.1 pomeni da odstranimo entitete, ki nimajo podatka scope-u
bad_score_scope|0.0

## class scope params ##
sub_levels|4
super_levels|10
max_size_subclasses|50

## synonims - iz izkušenj se z njimi lahko izognemo slabih sinonimov ##
syn_items_size|4
syn_items_size_for_allowed_addition|4
syn_items_max_addition_size|3
use_lammagen|true

onto_languages|en
#onto_languages|sl


## combine entities with class instances ##
combine_individuals|true

## uporaba sinonimov na nivoju instanc ##
use_umls_4_individuals|true
use_umls_4_context_individuals|true

## scope ##
use_neighbourhood_similarity|true
extract_scope_mapping|true


object_property_relation_URI|http://www.lavbic.net/onto/sakila/relatedTo
object_property_support_URI|http://www.lavbic.net/onto/sakila/SupportClass
object_property_support_URI|http://www.lavbic.net/onto/sakila/SupportClass
GS_identity_mappings|C://xampp//htdocs//FRI_MAG_RDFizing//GS_identity_mappings.rdf

######### END:DBAL #########

min_size_overlapping|15000
output_class_mappings|true
output_prop_mappings|false
instance_matching|true
output_instance_mappings|true
output_instance_mapping_files|true
reason_datatypes|true
#reasoner|Pellet
reasoner|MORe
#reasoner|HermiT
timeout|910000
reverse_labels=true
output_equivalences_only|false
second_chance_conflicts|true
ratio_second_chance_discarded|5
use_umls_lexicon|true
allow_interactivity|false


annotation_URI|http://www.w3.org/2000/01/rdf-schema#label



