<?php

class Test_model extends CI_Model {

    function __construct() {
        parent::__construct();
//        $this->load->database();
    }

    public function get_tables() {
        $query = $this->db->list_tables();

        return $query;
    }

    public function get_fields($table) {
        $fields = $this->db->field_data($table);

        return $fields;
    }

}
