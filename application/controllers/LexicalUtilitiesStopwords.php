<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LexicalUtilities
 *
 * @author dbalija
 */
class LexicalUtilitiesStopwords extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url', 'date', 'file');
        $this->load->dbutil();
        $this->load->database();
    }

    public function index() {
        $dbs = $this->dbutil->list_databases();

        $syn_file = LEX_STOPWORDS;

        $lexUtilsSynonyms = null;

        if (is_file($syn_file)) {
            $lexUtilsSynonyms = file_get_contents($syn_file, true);

//            $lexUtilsSynonyms = substr($lexUtilsSynonyms, 1, -1);
        } else {
            echo "The file $syn_file does not exist";
        }

        $data = array(
            'headerContent' => $this->load->view('include/main_header', array(), TRUE),
            'mainContent' => $this->load->view('settings_lexutils_view_stopwords', array('dbs' => $dbs, 'lexUtilsSynonyms' => json_encode($lexUtilsSynonyms)), TRUE),
            'footerContent' => $this->load->view('include/main_footer', array(), TRUE),
        );

        $this->load->view('templates/main_template', $data);
    }

    public function saveLexicalUtilsStopwords() {
        $this->load->helper('file');

        $data = $this->input->post('lexUtilsContent', true);

        $data = ltrim($data, '"');
        $data = rtrim($data, '"');

        if (!write_file('./' . LEX_STOPWORDS, $data)) {
            echo 'Unable to write the file';
        } else {
            echo 'File written!';
            redirect('mapping/index');
        }
    }

    //put your code here
}
