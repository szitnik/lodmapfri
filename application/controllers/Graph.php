<?php

class Graph {

    protected $graph;
    protected $visited = array();

    public function __construct($graph) {
        $this->graph = $graph;
    }

    public function breadthFirstSearch($start, $goal) {
        $queue = array();
        array_push($queue, array($start => array($start)));

        while (count($queue) > 0 /* && $queue->bottom() != $destination */) {

            $temp_vertex = array_pop($queue);
            $vertex = implode(array_keys($temp_vertex));
            $path = $temp_vertex[$vertex];

            if (!empty($this->graph[$vertex])) {

                $tmp_graph = array_diff($this->graph[$vertex], $path);

                foreach ($tmp_graph as $next) {

                    if ($next == $goal) {
                        print_r(array_merge($path, array($next)));
                        echo '<br />';
                    } else {
                        array_push($queue, array($next => array_merge($path, array($next))));
                    }
                }
            }
        }
    }

    public function breadthFirstSearchShortestPath($origin, $destination) {
        // mark all nodes as unvisited
        foreach ($this->graph as $vertex => $adj) {
            $this->visited[$vertex] = false;
        }

        // create an empty queue
        $q = new SplQueue();

        // enqueue the origin vertex and mark as visited
        $q->enqueue($origin);
        $this->visited[$origin] = true;

        // this is used to track the path back from each node
        $path = array();
        $path[$origin] = new SplDoublyLinkedList();
        $path[$origin]->setIteratorMode(
                SplDoublyLinkedList::IT_MODE_FIFO | SplDoublyLinkedList::IT_MODE_KEEP
        );

        $path[$origin]->push($origin);

        $found = false;
        // while queue is not empty and destination not found
        while (!$q->isEmpty() && $q->bottom() != $destination) {
            $t = $q->dequeue();

            if (!empty($this->graph[$t])) {
                // for each adjacent neighbor
                foreach ($this->graph[$t] as $vertex) {
                    if (!$this->visited[$vertex]) {
                        // if not yet visited, enqueue vertex and mark
                        // as visited
                        $q->enqueue($vertex);
                        $this->visited[$vertex] = true;
                        // add vertex to current path
                        $path[$vertex] = clone $path[$t];
                        $path[$vertex]->push($vertex);
                    }
                }
            }
        }

        if (isset($path[$destination])) {
            echo "$origin to $destination in ",
            count($path[$destination]) - 1,
            " hopsn";
            $sep = '';
            foreach ($path[$destination] as $vertex) {
                echo $sep, $vertex;
                $sep = '->';
            }
            echo "n";
        } else {
            echo "No route from $origin to $destinationn";
        }
    }

}
