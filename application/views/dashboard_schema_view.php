<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>

<!-- Modal -->

<?php if (count($schema_log)) { ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">ShemaSpy log</h4>

                </div>
                <div class="modal-body">
                    <pre>
                        <?php
                        foreach ($schema_log as $value) {
                            echo $value;
                            echo "<br />";
                        }
                        ?>

                    </pre>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
<?php } //END IF MODAL ?>
<!-- /.modal -->



<div class="row">
    <!-- Form Name -->
    <legend>Database Schema</legend>

    <!--<div class="col-md-8">-->
    <a href="<?php echo base_url(); ?>TEST/diagrams/summary/relationships.implied.large.png" class="thumbnail">
        <img src="<?php echo base_url(); ?>TEST/diagrams/summary/relationships.implied.compact.png" alt="testna shema db" style="max-height: 600px;">
        <!--<img data-src="holder.js/100%x180" alt="...">-->
    </a>

    <?php
    if (count($schema_log)) {
        echo '<a href="#myModal" role="button" class="btn btn-default btn-xs" data-toggle="modal">Schema converter log</a>';
    } //END IF MODAL 
    ?>
    <!--</div>-->
</div>


<!--<table class='table table-bordered'>
    <thead align='left'>
        <tr>
            <th>Column</th>
            <th>Type</th>
            <th>Size</th>
            <th title='Are nulls allowed?'>Nulls</th>
            <th title='Is column automatically updated?'>Auto</th>
            <th title='Default value'>Default</th>
            <th title='Columns in tables that reference this column'><span class='notSortedByColumn'>Children</span></th>
            <th title='Columns in tables that are referenced by this column'><span class='notSortedByColumn'>Parents</span></th>
            <th title='Comments' class='comment'><span class='notSortedByColumn'>Comments</span></th>
        </tr>
    </thead>
    <tbody valign='top'>
        <tr class='odd'>
            <td class='primaryKey' title='Primary Key'>actor_id</td>
            <td class='detail'>smallint unsigned</td>
            <td class='detail' align='right'>5</td>
            <td class='detail' align='center'></td>
            <td class='detail' align='center' title='Automatically updated by the database'>&nbsp;&radic;&nbsp;</td>
            <td class='detail'></td>
            <td class='detail'>
                <table border='0' width='100%' cellspacing='0' cellpadding='0'>
                    <tr class='impliedRelationship relative odd' valign='top'>
                        <td class='relatedTable detail' title="actor_info.actor_id's name implies that it's a child of actor.actor_id, but it doesn't reference that column."><a href='actor_info.html'>actor_info</a><span class='relatedKey'>.actor_id</span>
                        </td>
                        <td class='constraint detail'>Implied Constraint<span title='Restrict delete:
                                                                              Parent cannot be deleted if children exist'>&nbsp;R</span></td>
                    </tr>
                    <tr class='relative odd' valign='top'>
                        <td class='relatedTable detail' title="film_actor.actor_id refs actor.actor_id via fk_film_actor_actor"><a href='film_actor.html'>film_actor</a><span class='relatedKey'>.actor_id</span>
                        </td>
                        <td class='constraint detail'>fk_film_actor_actor<span title='Restrict delete:
                                                                               Parent cannot be deleted if children exist'>&nbsp;R</span></td>
                    </tr>
                </table>
            </td>
            <td class='detail'> </td>
            <td class='comment detail'></td>
        </tr>
        <tr class='even'>
            <td class='detail'>first_name</td>
            <td class='detail'>varchar</td>
            <td class='detail' align='right'>45</td>
            <td class='detail' align='center'></td>
            <td class='detail' align='center'></td>
            <td class='detail'></td>
            <td class='detail'></td>
            <td class='detail'> </td>
            <td class='comment detail'></td>
        </tr>
        <tr class='odd'>
            <td class='indexedColumn' title='Indexed'>last_name</td>
            <td class='detail'>varchar</td>
            <td class='detail' align='right'>45</td>
            <td class='detail' align='center'></td>
            <td class='detail' align='center'></td>
            <td class='detail'></td>
            <td class='detail'></td>
            <td class='detail'> </td>
            <td class='comment detail'></td>
        </tr>
        <tr class='even'>
            <td class='detail'>last_update</td>
            <td class='detail'>timestamp</td>
            <td class='detail' align='right'>19</td>
            <td class='detail' align='center'></td>
            <td class='detail' align='center'></td>
            <td class='detail' align='right'>CURRENT_TIMESTAMP</td>
            <td class='detail'></td>
            <td class='detail'> </td>
            <td class='comment detail'></td>
        </tr>
</table>-->


<div class="row">
    <legend></legend>

    <div class="col-md-8">
        <?php echo form_open('dashboard/getMappingFile', "class=form-horizontal", "action=post"); ?>
        <fieldset>


            <div class="form-group" id="tables_list">

                <label class="col-md-4 control-label" for="selectmultiple">Select tables from <?php echo $selected_db ?></label>

                <div class="col-md-8">
                    <select id="table_selection" name="table_list[]" class="lstTables" multiple="multiple" style="height: 250px;">
                        <?php
                        if (count($tables)) {
                            foreach ($tables as $table) {
                                echo "<option value='" . $table . "'>" . $table . "</option>";
                            }
                        }
                        ?>
                    </select>
                    <span class="help-block">Select tables to include in R2RML mapping file.</span> 
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="pull-right">
                        <button type="submit" id="getR2RML" class="btn btn-primary ">Get R2RML file</button>
                    </div>
                </div>
            </div>


        </fieldset>
        <!--</form>-->
        <?php echo form_close(); ?>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->


<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>


    $(function() {

//        $("#showDbFields").click(function(event) {
//            getDatabaseData();
//        });
//        
//        $("#getR2RML").click(function(event) {
//            getMappingFile();
//        });
    });

//    $(document).ready(function() {
//        $('#table_selection').change(function() {
//            $("#table_selection option:selected").each(function() {
//                alert($(this).val());
//            });
//        });
//    });


    function getDatabaseData() {

        $.ajax({url: "<?php echo site_url('dashboard/getDatabaseData'); ?>",
            data: {
                database_name: $("#selectDatabase option:selected").text(),
                database_username: $("#db_user").val(),
                database_password: $("#db_pass").val()},
            dataType: "json",
            type: "POST",
            success: function(data) {
                if (data.length == 0) {
                    alert("Data error!");
                } else {
//                    $("#db_fields").show();
//                    $("#db_fields").text(dump(data));
                    console.log(data);

//                    $("#tables_list").show();
                    $('#tables_data.hidden').css('visibility', 'visible').hide().fadeIn().removeClass('hidden');

                    $('#showDbFields').hide().fadeOut();

                    $.each(data, function(index, value) {
//                        ind = 0;
                        $('#table_selection').append('<option value="' + value + '">' + value + '</option>');
                    });

//                    $("#db_fields").append($.dump(data));
//                    console.log(data);
//                    alert("OK");
                }
            }
        });
    }

    function getMappingFile() {

//        var selected_tables = [];
//        $("#table_selection option:selected").each(function(index, value) {
//            selected_tables.push($(this).val());
//        });

        console.log(selected_tables);

        $.ajax({url: "<?php echo site_url('dashboard/getMappingFile'); ?>",
            data: {
                database_name: $("#selectDatabase option:selected").text(),
                selected_tables: selected_tables},
            dataType: "json",
            type: "POST",
            success: function(data) {
                if (data.length == 0) {
                    alert("Data error!");
                } else {
//                    $("#db_fields").show();
//                    $("#db_fields").text(dump(data));
                    console.log(data);

//                    $("#tables_list").show();
                    $('#tables_data.hidden').css('visibility', 'visible').hide().fadeIn().removeClass('hidden');

                    $('#showDbFields').hide().fadeOut();

                    $.each(data, function(index, value) {
                        $('#table_selection').append('<option value="' + value + '">' + value + '</option>');
                    });

//                    $("#db_fields").append($.dump(data));
//                    console.log(data);
//                    alert("OK");
                }
            }
        });
    }

    /**
     * Function : dump()
     * Arguments: The data - array,hash(associative array),object
     *    The level - OPTIONAL
     * Returns  : The textual representation of the array.
     * This function was inspired by the print_r function of PHP.
     * This will accept some data as the argument and return a
     * text that will be a more readable version of the
     * array/hash/object that is given.
     * Docs: http://www.openjs.com/scripts/others/dump_function_php_print_r.php
     */
    function dump(arr, level) {
        var dumped_text = "";
        if (!level)
            level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for (var j = 0; j < level + 1; j++)
            level_padding += "    ";

        if (typeof (arr) == 'object') { //Array/Hashes/Objects 
            for (var item in arr) {
                var value = arr[item];

                if (typeof (value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value, level + 1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Stings/Chars/Numbers etc.
            dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
        }
        return dumped_text;
    }


    // using JQUERY's ready method to know when all dom elements are rendered
    $(document).ready(function() {
        // set an on click on the button
        $("#button").click(function() {
            // get the time if clicked via an ajax get queury
            // see the code in the controller time.php
            $.get("/index.php/getTime", function(time) {
                // update the textarea with the time
                $("#text").html("Time on the server is:" + time);
            });
        });
    });
</script>
