<?php $this->load->helper('url'); ?>
<?php $this->load->helper('html'); ?>
<?php $this->load->helper('form'); ?>


<div class="container">

    <!--<div class="row well well-large">-->            
    <!--<div class="col-md-9">-->
    <!-- General Questions -->

    <div class="row">

        <div class="well well-lg">

            <?php
            $attributes = array('class' => 'R2RMLSourceForm', 'class' => 'form-horizontal');
            echo form_open('LexicalUtilities/saveLexicalUtilsSynonyms', $attributes);
            ?>
            <input type="hidden" name="database" value="" />

            <!--<div class="headline"><h2>Lexical Utilities: Creating and Editing Synonyms</h2></div>-->
            <legend>Lexical Utilities: Creating and Editing Synonyms </legend>
            <h3>
                <small></small>
            </h3>

            <br />

            <fieldset>
                <div class="form-group">
                    <div class="container">
                        <div class="col-md-12">
                            <textarea class="form-control" rows="15" id="lexUtilsSynonyms" name="lexUtilsSynonyms"></textarea>
                        </div>
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon glyphicon-floppy-disk"></span> Save </button>
                        </div>
                    </div>
                </div>
            </fieldset>

            <?php echo form_close(); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <small>
                        <p class="help-block">
                            <kbd>Ctrl-F</kbd> / <kbd>Cmd-F</kbd>
                            Start searching
                        </p>
                        <p class="help-block">
                            <kbd>Ctrl-G</kbd> / <kbd>Cmd-G</kbd>
                            Find next
                        </p>
                        <p class="help-block">
                            <kbd>Shift-Ctrl-G</kbd> / <kbd>Shift-Cmd-G</kbd>
                            Find previous
                        </p>
                        <p class="help-block">
                            <kbd>Shift-Ctrl-F</kbd> / <kbd>Cmd-Option-F</kbd>
                            Replace
                        </p>
                        <p class="help-block">
                            <kbd>Shift-Ctrl-R</kbd> / <kbd>Shift-Cmd-Option-F</kbd>
                            Replace all
                        </p>
                    </small>
                </div>
            </div>


        </div>
    </div>



    <!--</div>-->
</div>



<script>

    $(document).ready(function () {


        var editor = CodeMirror.fromTextArea(document.getElementById("lexUtilsSynonyms"), {
            mode: "text/x-q",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true
        });

        var data2 = '<?php print $lexUtilsSynonyms; ?>';
//        var data2 = "adfafasd";

//    console.log(data2);

//        $('input[name="ttlContent4RDF"]').val(editor.getValue());
//        var editor = $('.CodeMirror')[0].CodeMirror;
        editor.setValue(data2);
    });

</script>

