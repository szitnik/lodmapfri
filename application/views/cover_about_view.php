<?php $this->load->helper('url'); ?>

<h1 class="cover-heading">LodMapFRI - RDB Data Enrichment Tool.</h1>

<p class="lead">Enrichment of data in relational database with Linked Data. In the process of enrichment are used different approaches and tools such as: R2RML, db2triples, schemaSPY, LogMapFRI...</p>

<p class="lead">
    <a href="<?php echo base_url("index.php/mapping"); ?>" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Start LodMapFRI workflow</a>
</p>



